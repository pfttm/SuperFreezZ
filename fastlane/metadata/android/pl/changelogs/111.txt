Dodano EKSPERYMENTALNĄ obsługę roota
Zaktualizowano tłumaczenia

Odwrócono kolejność sortowania w stanie zamrożenia, zobacz #83
Wycofano "Poprawka #67 Sortuj rosnąco/malejąco"; poprawka #83
Naprawiono skróty do wersji debugowania
