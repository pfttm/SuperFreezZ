UWAGA: Wykonałem dużo refaktoryzacji, więc mogą być problemy! Jeśli SuperFreezZ jest dla Ciebie bardzo ważny, poczekaj na wersję 0.5.

Prosimy o zgłaszanie wszelkich problemów na https://gitlab.com/SuperFreezZ/SuperFreezZ/issues

Zaktualizowano tłumaczenia
Ulepszenia w wydajności
Crashfix: Podczas uruchamiania: android.view.WindowManager$BadTokenException
MainActivity nie wyświetla się po zamrożeniu
Bugfix: Czasami limit czasu ekranu pozostawał na 15s po zatrzymaniu się na ekranie: Poprawka #32
